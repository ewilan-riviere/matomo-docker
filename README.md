# Matomo Docker

[![matomo][matomo-version-src]][matomo-version-href]

Matomo, formerly Piwik, is the most common free and open source web analytics application to track online visits to one or more websites and display reports on these visits for analysis.

- [Matomo on GitHub](https://github.com/matomo-org/matomo)
- [Matomo with NGINX](https://github.com/matomo-org/matomo-nginx)
- [Matomo with Docker](https://github.com/matomo-org/docker)

## Docker

> Tested for Matomo v5.1.0

Create a `.env` file from `.env.example` and change the values.

```bash
cp .env.example .env
```

MySQL variables

- `MYSQL_ROOT_PASSWORD`: password
- `MYSQL_DATABASE`: matomo_db
- `MYSQL_USER`: matomo_user
- `MYSQL_PASSWORD`: password
- `MARIADB_AUTO_UPGRADE`: 1
- `MARIADB_INITDB_SKIP_TZINFO`: 1

Matomo variables

- `MATOMO_DATABASE_HOST`: db
- `MATOMO_DATABASE_DBNAME`: matomo_db
- `MATOMO_DATABASE_USERNAME`: matomo_user
- `MATOMO_DATABASE_PASSWORD`: password

Other variables

- `CONTAINER_RESTART`: restart policy for the container (`no`, `always`, `unless-stopped`, `on-failure`, `always`)
- `APP_PORT`: 8080

Build and run the docker compose

```bash
docker compose down
docker compose up -d
```

Matomo is now running on <http://localhost:8080> (default port).

You can use [this template](./nginx/nginx.conf) to host with NGINX, adapt to your needs.

## Options

### Bash

Execute bash in the app container

```bash
docker container exec -it matomo /bin/sh
```

### Logs

Check logs

```bash
docker logs matomo # docker logs matomo -f for live logs
```

[matomo-version-src]: https://img.shields.io/static/v1?style=flat&label=Matomo&message=v5.1.0&color=3152A0&logo=matomo&logoColor=ffffff&labelColor=18181b
[matomo-version-href]: https://matomo.org/
